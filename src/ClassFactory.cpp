static const char *RcsId = "$Id: ClassFactory.cpp,v 1.1 2011-09-28 13:21:00 olivierroux Exp $";
//+=============================================================================
//
// file :        ClassFactory.cpp
//
// description : C++ source for the class_factory method of the DServer
//               device class. This method is responsible for the creation of
//               all class singleton for a device server. It is called
//               at device server startup
//
// project :     TANGO Device Server
//
// $Author: olivierroux $
//
// $Revision: 1.1 $
// $Date: 2011-09-28 13:21:00 $
//
// SVN only:
// $HeadURL: $
//
// CVS only:
// $Source: /users/chaize/newsvn/cvsroot/Motion/OrthodromicPositioner/src/ClassFactory.cpp,v $
// $Log: not supported by cvs2svn $
//
// copyleft :    European Synchrotron Radiation Facility
//               BP 220, Grenoble 38043
//               FRANCE
//
//-=============================================================================
//
//  		This file is generated by POGO
//	(Program Obviously used to Generate tango Object)
//
//         (c) - Software Engineering Group - ESRF
//=============================================================================


#include <tango.h>
#include <OrthodromicPositionerClass.h>

/**
 *	Create OrthodromicPositionerClass singleton and store it in DServer object.
 */

void Tango::DServer::class_factory()
{

	add_class(OrthodromicPositioner_ns::OrthodromicPositionerClass::init("OrthodromicPositioner"));

}
